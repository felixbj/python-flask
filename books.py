books = [
    { '_id':1,'title':'titulo del libro I','pages':123,'edition':1,'price':4.99,'published':'1975' },
    { '_id':2,'title':'titulo del libro II','pages':123,'edition':1,'price':4.99,'published':'1976' },
    { '_id':3,'title':'titulo del libro III','pages':123,'edition':1,'price':4.99,'published':'1977' }
]

def getAll():
    return books
def getOne(bookId):
    booksFound = [book for book in books if book['_id'] == bookId]
    return booksFound
def createOne(inputsBook):
    book = inputsBook
    book["_id"] = books[len(books)-1]["_id"]+1
    books.append(book)
    return book
def editOne(booksFound,bookEdited):
    for key in bookEdited:
        booksFound[0][key] = bookEdited[key]
    return booksFound
def deleteOne(booksFound):
    books.remove(booksFound[0])
    return True