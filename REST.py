
from flask import jsonify

def OK(message,data):
    content = {'status_code':200}
    if (message):
        content['message'] = message
    else:
        content['message'] = 'ok'
    if (data):
        content['data'] = data
    return jsonify(content),200
def Created(data):
    content = {'status_code':201}
    content['message'] = 'Successfully registered'
    content['data'] = data
    return jsonify(content),201
def NoContent():
    content = {'status_code':204}
    content['message'] = 'No Content'
    return jsonify(content),204
def RecordNotFound(message):
    content = {'status_code':404}
    if (message):
        content['message'] = message
    else:
        content['message'] = 'Record not found'
    return jsonify(content),404
def Conflict(message,info):
    content = {'status_code':409}
    if (message):
        content['message'] = message
    else:
        content['message'] = 'Conflict'
    if (info):
        content['info'] = info
    return jsonify(content),409
