## About Projec
Simple APi using Python and its web framework Flask. No conection DataBase

## Installation
Installation process in ubuntu 20.04

```bash
$ python3 -m venv venv
$ . venv/bin/activate
$ pip3 install Flask
```

## Running the app

```bash
$ . venv/bin/activate
$ python app.py
```