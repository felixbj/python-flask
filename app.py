from flask import Flask,request,jsonify
import REST
import books as Book

app = Flask(__name__)

@app.route('/')
def hello_world():
    return REST.OK('Hello, World!',None)
@app.route('/books',methods=['GET'])
def getAllBooks():
    books = Book.getAll()
    return  REST.OK(None,books)
@app.route('/books/<int:bookId>',methods=['GET'])
def getOneBooks(bookId):
    book = Book.getOne(bookId)
    if (not len(book)): return REST.RecordNotFound(None)
    return  REST.OK(None,book)
@app.route('/books',methods=['POST'])
def createOneBook():## Validate request
    return  REST.Created(Book.createOne(request.json))
@app.route('/books/<int:bookId>',methods=['PUT'])
def editOneBook(bookId):## Validate request
    book = Book.getOne(bookId)
    if (not len(book)): return REST.RecordNotFound(None)
    return  REST.OK(None,Book.editOne(book,request.json))
@app.route('/books/<int:bookId>',methods=['DELETE'])
def deleteOneBook(bookId):
    book = Book.getOne(bookId)
    if (not len(book)): return REST.RecordNotFound(None)
    if (not Book.deleteOne(book)): return REST.Conflict(None,None)
    return  REST.NoContent()

if __name__=="__main__":
    app.run(debug=True,port=4000)